import 'package:flutter/material.dart';

class  LabListview extends StatelessWidget {
  
  List<String> nombres = [
    'Matias Taylor',
    'Staicy Collins',
    'Dona Polcent',
    'Kevin Mcgregor',
    'Matt Tompson',
    'Joseph Gambardello',
    'Harvey Specter',
    'Mike Ross'
  ];

   LabListview ({Key? key}) : super(key: key);

  @override
  Widget  build(BuildContext context) {
    return  Scaffold(
        appBar:  AppBar(
        title: const Text('ListView'),
        ),
        body:  Center(
          child:  ListView(
            children: [
              Text(nombres[0]),
              Divider(),
              Text(nombres[1]),
              Divider(),
              Text(nombres[2]),
              Divider(),
              Text(nombres[3]),
              Divider(),
              Text(nombres[4]),
              Divider(),
              Text(nombres[5]),
              Divider(),
              Text(nombres[6]),
              Divider(),
              Text(nombres[7]),
            ],
          ),
        )
      );
  }
}
