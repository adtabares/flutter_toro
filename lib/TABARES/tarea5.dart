import 'package:flutter/material.dart';
import 'package:flutter_toro/utils/button_style.dart';

import 'navegacion.dart';

class Tarea5 extends StatelessWidget {
  const Tarea5({Key? key,  this.data}) : super(key: key);
final String? data;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pushNamed(
            context,
            'navegacionAPage',
          ),
        ),
        title: const Text("Tarea 5"),
      ),
      body: Center(
        child: TextButton(
            style: textButtonStyle,
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder:(_) =>const  NavegacionAPage())),
            child:  Text(data!)),
      ),
    );
  }
}
