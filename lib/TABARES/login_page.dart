import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_toro/TABARES/menu_page.dart';
import 'package:flutter_toro/provider/name_provider.dart';
import 'package:flutter_toro/utils/button_style.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final vocal = Provider.of<NamesProviders>(context);
    String valorEmail = '';
    String valorPassword = '';
    int clave = 123;
    int val = 0;
    int cadena = 0;
    String expresionRegular = '';

    return Scaffold(
      appBar: AppBar(
        title: const Text('Login Page'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: TextField(
                  controller: controllerEmail,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      hintText: 'User_Name',
                      labelText: 'Email',
                      ),
                  onChanged: (value) {
                    valorEmail = value;
                    print(valorEmail);
                    
                    
                  },
                ),
              ),
              const SizedBox(
                height: 50.0,
              ),
              TextField(
                controller: controllerPassword,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    hintText: 'Keypass',
                    labelText: 'Password'),
                onChanged: (value) {
                  valorPassword = value;

                  print(valorPassword);
                },
              ),
              const SizedBox(
                height: 50.0,
              ),
              TextButton(
                  style: textButtonStyle,
                  child: const Text('Enter'),
                  onPressed: () {
                    
                   // valorEmail = controllerEmail.text;

                    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
 
                    RegExp regExp  =  RegExp(pattern);

                   // && regExp.hasMatch(valorEmail)


                    setState(() {
                      valorEmail = controllerEmail.text;
                      valorPassword = controllerPassword.text;
                      cadena = valorPassword.toString().length;
                      // val = int.parse(valorPassword);
                      //clave = val;

                      // print(valorEmail);
                      // print(valorPassword);
                      log(valorEmail);
                      log(valorPassword);
                    });
                    if (valorEmail.isNotEmpty &&
                        valorPassword.isNotEmpty &&
                        (cadena > 0 && cadena < 4) && regExp.hasMatch(valorEmail) ){
                          vocal.addName(valorEmail);

                        
                          
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MenuPage()));
                    } else {
                      showDialog(
                        context: context,
                        builder: (context) =>  AlertDialog(
                          title: Text(vocal.names[0]),
                          content:
                            const   Text('Por favor ingrese los datos solicitados'),
                        ),
                      );
                    }
                    
                  })
            ],
          ),
        ),
      ),
    );
  }
}
