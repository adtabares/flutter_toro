import 'package:flutter/material.dart';
import 'package:flutter_toro/TABARES/tarea4.dart';
import 'package:flutter_toro/utils/button_style.dart';

class NavegacionAPage extends StatelessWidget {
  const NavegacionAPage({Key? key, this.data}) : super(key: key);
final String? data;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Navegacion a page"),
      ),
      body: Center(
        child: TextButton(
          style: textButtonStyle,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) => const Tarea4(data: 'data'))
            );
            },
        child:const Text('Toro')),
      ),
    );
  }
}