import 'package:flutter/material.dart';
import 'package:flutter_toro/Texts/text1.dart';

class Tarea1TabaresPage extends StatelessWidget {
  const Tarea1TabaresPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        title: const Text('tarea'),
        actions: [
          
          IconButton(
            icon: const Icon(Icons.remove_done),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=> const Text1())),
            //child: const Icon(Icons.remove),
          ),

          IconButton(
            icon:  const Icon(Icons.add_alert_rounded),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context)=> const Text1())),
            //child: const Icon(Icons.remove),
          ),
        ],
      ),
      body: const Center(
        child: Text('Tarea'),
      ),
    );
  }
}
