import 'package:flutter/material.dart';
import 'package:flutter_toro/TABARES/tarea5.dart';
import 'package:flutter_toro/utils/button_style.dart';

class Tarea4 extends StatelessWidget {
  const Tarea4({Key? key, this.data = ''}) : super(key: key);
  final String? data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tarea 4"),
      ),
      body: Center(
        child: TextButton(
          style: textButtonStyle,
          onPressed: () { 
            Navigator.push(context, MaterialPageRoute(builder: (_) =>  Tarea5(data: data)));
           },
        child:  Text(data!)),
      ),
    );
  }
}