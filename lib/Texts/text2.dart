import 'package:flutter/material.dart';
import 'package:flutter_toro/utils/button_style.dart';

class Screen2 extends StatelessWidget {
  const Screen2({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Screen 2'),
      ),
      body:  Column(
        children: [
           const Center(
            child:  Text('SCREEN 2'),

            
          ),
          TextButton(
              style: textButtonStyle,
              onPressed: () {  },
              child: const Text('Regresar'),
              )
        ],
      ),
      
    );
  }
}