import 'package:flutter/material.dart';
import 'package:flutter_toro/Texts/text1.dart';
import 'package:flutter_toro/utils/button_style.dart';

class Text3 extends StatelessWidget {
  final String? texto;
  const Text3({Key? key, this.texto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Text3'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(texto!),
            const SizedBox(height: 30.0),
            TextButton(
              style: textButtonStyle,
              child: const Text('Button'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Text1(texto3: texto),
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
