import 'package:flutter/material.dart';
import 'package:flutter_toro/Texts/text2.dart';
import 'package:flutter_toro/Texts/text3.dart';
import 'package:flutter_toro/utils/button_style.dart';

class Text1 extends StatefulWidget {
  final String? texto3;
  const Text1({
    Key? key,
    this.texto3 = '',
  }) : super(key: key);

  @override
  _Text1State createState() => _Text1State();
}

class _Text1State extends State<Text1> {
  TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    
    String valor = '';
    bool bandera = false;
    
    return Scaffold(
      appBar: AppBar(
        
        leading: IconButton(
          icon: const Icon(Icons.add_box_sharp),
          onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Screen2()),
          ),
        ),
        centerTitle: true,
        title: const Text('Text1'),
        actions: [
          IconButton(
            onPressed: () {
              controller.text = '';
            },
            icon: const Icon(Icons.remove_done_sharp),
          )
        ],
      ),
      body: Center(
        //Aqui vienen los style del text:
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //
              TextField(
                controller: controller,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  hintText: 'Nombre',
                  labelText: 'Password',
                ),
                onChanged: (value) {
                  valor = value;
                  print('Cada caracter escrito pasa por aqui$value');
                  //   controller.text = value;
                  // value = '';
                },
              ),
              //A
              //

              const SizedBox(height: 40.0),

              ///
              ///

              TextButton(
                style: textButtonStyle,
                child: const Text('Button'),
                onPressed: () {
                  setState(
                    () {
                      bandera = true;
                      valor = controller.text;
                    },
                  );
                  if (valor.isNotEmpty) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Text3(texto: valor)));
                  } else {
                    /*  Future.delayed(const Duration(seconds: 3), () {
                      Navigator.of(context).pop();
                    }); */
                    showDialog(
                      //barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: const Text('Porfavor ingrese un caracter'),
                          content: TextButton(
                            style: textButtonStyle,
                            child: const Text('Salir'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        );
                      },
                    );
                  }

                  //valor = controller.text;

                  //print(valor);
                },
              ),

              ///

              const SizedBox(height: 30.0),
              //
              Text(
                controller.text,
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w800,
                  color: Colors.black,
                ),
              ),

              //Text(widget.texto3!)
            ],
          ),
        ),
      ),
    );
  }
}
