import 'package:flutter/material.dart';

class Button1 extends StatelessWidget {
  const Button1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Button1'),
      ),
      body: Center(
        child: TextButton(
          //aqui viene el style:  del button
          child: const Text('Button'),
          onPressed: () {},
        ),
      ),
    );
  }
}
