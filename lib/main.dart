import 'package:flutter/material.dart';
import 'package:flutter_toro/provider/name_provider.dart';
import 'package:flutter_toro/routes/routes.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => NamesProviders(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Material App',
          // initialRoute: 'mainPage',
          //initialRoute: 'mainPage',
          // initialRoute: 'loginpage',
          initialRoute: 'navegacionAPage',
          routes: appRoutes,
        ));
  }
}
