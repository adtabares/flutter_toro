import 'package:flutter/material.dart';

class Future1 extends StatelessWidget {
  const Future1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Future1'),
      ),
      body:const Center(
        child: Text('Future1'),
      ),
    );
  }
}
