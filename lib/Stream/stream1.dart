import 'package:flutter/material.dart';

class Stream1 extends StatelessWidget {
  const Stream1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Stream1'),
      ),
      body: const Center(
        child: Text('Stream1'),
      ),
    );
  }
}
