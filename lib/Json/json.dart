import 'package:flutter/material.dart';

class JsonPage extends StatelessWidget {
  const JsonPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('JsonPage'),
      ),
      body: const Center(
        child: Text('JsonPage'),
      ),
    );
  }
}
