import 'package:flutter/material.dart';
import 'package:flutter_toro/TABARES/menu_page.dart';
import 'package:flutter_toro/TABARES/tarea1_page.dart';
import 'package:flutter_toro/TABARES/tarea2_page.dart';
import 'package:flutter_toro/TABARES/login_page.dart';

import 'package:flutter_toro/pages/main_page.dart';

import '../TABARES/navegacion.dart';
import '../TABARES/tarea4.dart';
import '../TABARES/tarea5.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'mainPage'          : (_) => const MainPage(),
  'tarea1TabaresPage' : (_) => const Tarea1TabaresPage(),
  'tarea2page'        : (_) =>    LabListview(),
  'loginpage'         : (_) => const LoginPage(),
  'menuPage'          : (_) => const MenuPage(),
  'tarea4'            : (_) => const Tarea4(),
  'tarea5'            : (_) => const Tarea5(),
  'navegacionAPage'            : (_) => const NavegacionAPage(),

};
